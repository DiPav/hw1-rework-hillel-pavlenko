package hw1rework.Services;

import hw1rework.Unit.Dices;
import hw1rework.Unit.Player;

public class Game {
    private final Dices dice;
    private final Ui ui;

    public Game(Dices dice, Ui ui) {
        this.dice = dice;
        this.ui = ui;
    }

    public void run() {
        Player[] players = ui.readPlayers();
        do {
            throwDices(players);
            Player[] winners = getWinners(players);
            ui.showWinners(winners);
        } while (ui.askRepeat());
    }

    private Player[] getWinners(Player[] players) {
        int maxDice = getMaxThrownDice(players);
        int count = getNumberOfPlayersWithResult(players, maxDice);
        Player[] winners = new Player[count];
        for (int i = 0, j = 0; i < players.length; i++) {
            if (players[i].getLastThrownDice() == maxDice) winners[j++] = players[i];
        }
        return winners;
    }

    private int getNumberOfPlayersWithResult(Player[] players, int maxDice) {
        int count = 0;
        for (Player player : players) {
            if (player.getLastThrownDice() == maxDice) count++;
        }
        return count;
    }

    private int getMaxThrownDice(Player[] players) {
        int max = players[0].getLastThrownDice();
        for (Player player : players) {
            if (player.getLastThrownDice() > max) max = player.getLastThrownDice();
        }
        return max;
    }

    private void throwDices(Player[] players) {
        for (Player player : players) {
            player.setLastThrownDice(dice.throwDice());
        }
    }


}
