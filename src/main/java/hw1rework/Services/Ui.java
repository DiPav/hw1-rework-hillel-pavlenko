package hw1rework.Services;

import hw1rework.Unit.Player;

import java.util.Scanner;

public class Ui {
    public static final String RESET_AGREE_RESPONSE = "y";
    private final Scanner scanner;

    public Ui(Scanner scanner) {
        this.scanner = scanner;
    }

    public Player[] readPlayers() {
        int playersCount = askNumberOfPlayers();
        Player[] players = new Player[playersCount];
        for (int i = 0; i < playersCount; i++) {
            players[i] = askPlayerName(i + 1);
        }
        return players;
    }

    private Player askPlayerName(int numberOfPlayer) {
        System.out.printf("Enter %d player name: ", numberOfPlayer);
        return new Player(scanner.nextLine());
    }

    private int askNumberOfPlayers() {
        System.out.println("How much people is playing?: ");
        int count = scanner.nextInt();
        scanner.nextLine();
        return count;
    }

    public void showWinners(Player[] winners) {
        System.out.println("Winners:");
        for (Player winner : winners) {
            showPlayer(winner);
        }
    }

    private void showPlayer(Player player) {
        System.out.println("Player " + player.getName() + " that got " + player.getLastThrownDice());
    }

    public boolean askRepeat() {
        System.out.println("Restart?(Y/N)");
        return scanner.nextLine().equalsIgnoreCase(RESET_AGREE_RESPONSE);
    }
}
