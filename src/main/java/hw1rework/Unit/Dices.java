package hw1rework.Unit;

public class Dices {
    private static final double MAX = 6;
    private static final double MIN = 1;

    public int throwDice() {
        return (int) Math.round(Math.random() * (MAX - MIN) + MIN);
    }
}
