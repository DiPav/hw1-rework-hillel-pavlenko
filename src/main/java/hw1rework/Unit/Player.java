package hw1rework.Unit;

public class Player {
    private final String name;
    private int lastThrownDice;

    public Player(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getLastThrownDice() {
        return lastThrownDice;
    }

    public void setLastThrownDice(int lastThrownDice) {
        this.lastThrownDice = lastThrownDice;
    }
}
