package hw1rework;

import hw1rework.Services.Game;
import hw1rework.Services.Ui;
import hw1rework.Unit.Dices;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Ui ui = new Ui(new Scanner(System.in));
        Dices dice = new Dices();
        Game game = new Game(dice,ui);
        game.run();
    }
}